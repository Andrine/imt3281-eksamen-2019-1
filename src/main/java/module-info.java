module no.ntnu.imt3281.eksamen {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.json;
    requires java.logging;

    opens no.ntnu.imt3281.eksamen to javafx.fxml;
    opens no.ntnu.imt3281.eksamen.exchange_rates to javafx.fxml;
    exports no.ntnu.imt3281.eksamen;
}