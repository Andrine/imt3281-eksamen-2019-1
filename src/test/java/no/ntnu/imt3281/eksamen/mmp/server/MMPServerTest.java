package no.ntnu.imt3281.eksamen.mmp.server;

import org.json.JSONObject;
import org.junit.Test;

import java.io.*;
import java.util.Arrays;

import static org.junit.Assert.*;

public class MMPServerTest {
    /**
     * Test adding client to server when server started with require uname/pwd set to false.
     * When sending the correct protocol/version the server should respond with connected: OK.
     * When sending incorrect protocol or version information the server should respond
     * with a message indicating what is wrong.
     */
    @Test
    public void addClient() {
        MMPServer server = new MMPServer(false);
        JSONObject req = new JSONObject();
        req.put("protocol", "MMP");
        req.put("version", 0.1);
        // Attempt to connect, everything OK
        JSONObject[] json = runRequest(server, req.toString().getBytes());
        assertEquals("OK", json[0].getString("connected"));

        req.put("protocol", "MQTT");
        // Attempt to connect, bad protocol name
        json = runRequest(server, req.toString().getBytes());
        assertEquals("FAIL", json[0].getString("connected"));
        assertEquals("Bad protocol/protocol version", json[0].getString("reason"));

        req.put("protocol", "MMP");
        req.put("version", 1);
        // Attempt to connect, bad protocol version
        json = runRequest(server, req.toString().getBytes());
        assertEquals("FAIL", json[0].getString("connected"));
        assertEquals("Bad protocol/protocol version", json[0].getString("reason"));
    }

    /**
     * Test adding client to server when server started with require uname/pwd set to true.
     * When sending the correct protocol/version and username/password the server should respond with connected: OK.
     * When sending a connection message with missing username/password information or bad username/password information
     * the server should respond with a message indicating what is wrong.
     */
    @Test
    public void addClientWithUnamePwd() {
        MMPServer server = new MMPServer(true);
        JSONObject req = new JSONObject();
        req.put("protocol", "MMP");
        req.put("version", 0.1);
        // Server started with require uname/pwd, but no uname/pwd provided
        JSONObject[] json = runRequest(server, req.toString().getBytes());
        assertEquals("FAIL", json[0].getString("connected"));
        assertEquals("Bad username/password", json[0].getString("reason"));

        JSONObject user = new JSONObject();
        user.put("uname", "kåre");
        user.put("pwd", "hemmelig");
        req.put("user", user);
        // Server started with require uname/pwd, but bad uname/pwd provided
        json = runRequest(server, req.toString().getBytes());
        assertEquals("FAIL", json[0].getString("connected"));
        assertEquals("Bad username/password", json[0].getString("reason"));

        user.put("uname", "very");
        user.put("pwd", "secret");
        // Server started with require uname/pwd, good uname/pwd provided
        json = runRequest(server, req.toString().getBytes());
        assertEquals("OK", json[0].getString("connected"));
    }

    /**
     * Testing connecting and then disconnecting from the server.
     * When sending a message telling the server that the client wants to disconnect the server should respond with
     * a message confirming that the client is disconnected.
     * @throws InterruptedException
     */
    @Test
    public void disconnect() throws InterruptedException {
        MMPServer server = new MMPServer(false);
        JSONObject connect = new JSONObject();
        connect.put("protocol", "MMP");
        connect.put("version", 0.1);

        StringBuilder sb = new StringBuilder();
        sb.append(connect.toString());          // First send a json object with connect information
        sb.append("\n");

        JSONObject req = new JSONObject();
        req.put("action", "disconnect");
        sb.append(req.toString());              // Then send a disconnect request
        sb.append("\n");
        // Attempt to connect, everything OK
        JSONObject[] json = runRequest(server, sb.toString().getBytes());
        assertEquals("OK", json[0].getString("connected"));     // Should receive a connect confirmation
        assertEquals("OK", json[1].getString("disconnected"));  // then a disconnect confirmation
    }

    /**
     * Testing subscribing to a channel/topic.
     * When sending a subscribe message to the server the server should respond with a message confirming that
     * the client has been subscribed to that channel.
     */
    @Test
    public void subscribe() {
        MMPServer server = new MMPServer(false);
        JSONObject connect = new JSONObject();
        connect.put("protocol", "MMP");
        connect.put("version", 0.1);

        StringBuilder sb = new StringBuilder();
        sb.append(connect.toString());          // First send a json object with connect information
        sb.append("\n");

        JSONObject req = new JSONObject();
        req.put("action", "subscribe");         // Then send a subscribe request
        req.put("topic", "testchannel");
        sb.append(req.toString());
        sb.append("\n");

        req = new JSONObject();
        req.put("action", "disconnect");
        sb.append(req.toString());              // Then send a disconnect request
        sb.append("\n");
        // Attempt to connect, everything OK
        JSONObject[] json = runRequest(server, sb.toString().getBytes());
        assertEquals("OK", json[0].getString("connected"));     // Should receive a connect confirmation
        assertEquals("testchannel", json[1].getString("subscribed"));
        assertEquals("OK", json[2].getString("disconnected"));  // then a disconnect confirmation
    }

    /**
     * Testing that we can publish a message to a channel and also that if we are subscribed to that channel the
     * message will be sent back to the client.
     * First a message is sent to a channel with no subscribers to check that we get a correct response.
     * Then the client is subscribed to a channel and a new message is sent to the same channel as the client
     * has subscribed to to check both for correct response on the publish message and that the message is actually
     * sent to the client. NOTE, the server first sends the response to all clients, then sends a message to the client
     * sending the message saying that the message was actually sent.
     */
    @Test
    public void publish() {
        MMPServer server = new MMPServer(false);
        JSONObject connect = new JSONObject();
        connect.put("protocol", "MMP");
        connect.put("version", 0.1);

        StringBuilder sb = new StringBuilder();
        sb.append(connect.toString());          // First send a json object with connect information
        sb.append("\n");

        JSONObject req = new JSONObject();
        req.put("action", "publish");           // Publish to a channel with no subscribers
        req.put("topic", "testchannel");
        req.put("subject", "Testing");
        sb.append(req.toString());
        sb.append("\n");

        req = new JSONObject();
        req.put("action", "subscribe");         // Then send a subscribe request
        req.put("topic", "testchannel");
        sb.append(req.toString());
        sb.append("\n");

        req = new JSONObject();
        req.put("action", "publish");           // Publish to a channel with one subscriber
        req.put("topic", "testchannel");
        req.put("subject", "Testing");
        sb.append(req.toString());
        sb.append("\n");

        req = new JSONObject();
        req.put("action", "disconnect");
        sb.append(req.toString());              // Then send a disconnect request
        sb.append("\n");
        // Attempt to connect, everything OK
        JSONObject[] json = runRequest(server, sb.toString().getBytes());
        assertEquals("OK", json[0].getString("connected"));     // Should receive a connect confirmation
        assertEquals("testchannel", json[1].getString("published"));    // Message published
        assertEquals(0, json[1].getInt("recipients"));                  // No recipients, no one has subscribed
        assertEquals("testchannel", json[2].getString("subscribed"));   // Subscribed to channel
        assertTrue(json[3].has("message"));
        assertEquals("testchannel", json[3].getJSONObject("message").getString("topic"));   // Message received
        assertEquals("Testing", json[3].getJSONObject("message").getString("subject"));     // channel name
        assertEquals("testchannel", json[4].getString("published"));    // Message published
        assertEquals(1, json[4].getInt("recipients"));                  // One subscriber
        assertEquals("OK", json[5].getString("disconnected"));  // then a disconnect confirmation
    }

    /**
     * Much the same as the above, but using two clients to check that the response on the publish message is correct.
     * Connects two clients and subscribe to the same channel, then send a message to that channel to see if the
     * acknowledgement message is correct.
     */
    @Test
    public void multiClient() {
        MMPServer server = new MMPServer(false);
        JSONObject connect = new JSONObject();
        connect.put("protocol", "MMP");
        connect.put("version", 0.1);

        StringBuilder sb = new StringBuilder();
        sb.append(connect.toString());          // First send a json object with connect information
        sb.append("\n");

        JSONObject req = new JSONObject();
        req.put("action", "subscribe");         // Then send a subscribe request
        req.put("topic", "testchannel");
        sb.append(req.toString());
        sb.append("\n");

        JSONObject[] res = runRequest(server, sb.toString().getBytes());    // First client connects and subscribe to channel

        sb = new StringBuilder();
        sb.append(connect.toString());          // First send a json object with connect information
        sb.append("\n");

        req = new JSONObject();
        req.put("action", "subscribe");         // Then send a subscribe request
        req.put("topic", "testchannel");
        sb.append(req.toString());
        sb.append("\n");

        req = new JSONObject();
        req.put("action", "publish");           // Publish to a channel with two subscribers
        req.put("topic", "testchannel");
        req.put("subject", "Testing");
        sb.append(req.toString());
        sb.append("\n");

        JSONObject[] json = runRequest(server, sb.toString().getBytes());
        assertEquals("OK", json[0].getString("connected"));     // Should receive a connect confirmation
        assertEquals("testchannel", json[1].getString("subscribed"));   // Subscribed to channel
        assertTrue(json[2].has("message"));
        assertEquals("testchannel", json[2].getJSONObject("message").getString("topic"));   // Message received
        assertEquals("Testing", json[2].getJSONObject("message").getString("subject"));     // channel name
        assertEquals("testchannel", json[3].getString("published"));    // Message published
        assertEquals(2, json[3].getInt("recipients"));                  // Two subscribers

    }

    /**
     * Used to add a client to the server, and add the provided request(s) to the reader that the server will read
     * client requests from. Waits 10 milliseconds after the client has been added and then read any response(s)
     * from the server and converts them to JSON objects that is returned.
     *
     * @param server what server to connect the fake client to
     * @param req what data to "send" to the server, ie. that data is put onto the reader that the server will read client request from.
     * @return an array of JSON objects with the response from the server.
     */
    private JSONObject[] runRequest(MMPServer server, byte[] req) {
        ByteArrayInputStream bais = new ByteArrayInputStream(req);
        BufferedReader br = new BufferedReader(new InputStreamReader(bais));
        ByteArrayOutputStream baos = new ByteArrayOutputStream(2048);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(baos));
        server.addClient(new MMPServer.Client(bw, br));
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String[] res = baos.toString().split("\n");
        JSONObject[] json = new JSONObject[res.length];
        for (int i=0; i<res.length; i++) {
            json[i] = new JSONObject(res[i]);
        }
        return json;
    }
}